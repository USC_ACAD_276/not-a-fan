package finalproject.acad276.finalproject;

import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import java.util.ArrayList;


public class SecondActivity extends AppCompatActivity{

    TextView levisStadiumTextView;
    Spinner sectionDropdown;
    Spinner rowDropdown;
    Spinner seatDropdown;
    Button nextScreenTwoButton;
    String stadiumIndexString;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen2);

        Intent intent = getIntent();

        stadiumIndexString = intent.getStringExtra("stadiumIndex");
        final int stadiumIndex = Integer.parseInt(stadiumIndexString);

        String [] stadiums = getResources().getStringArray(R.array.stadiums);

        sectionDropdown = (Spinner)findViewById(R.id.sectionDropdown);
        ArrayAdapter<String> sectionAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.sections));
        sectionDropdown.setAdapter(sectionAdapter);

        rowDropdown = (Spinner)findViewById(R.id.rowDropdown);
        ArrayAdapter<String> rowAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.rows));
        rowDropdown.setAdapter(rowAdapter);

        seatDropdown = (Spinner)findViewById(R.id.seatDropdown);
        ArrayAdapter<String> seatAdapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.seats));
        seatDropdown.setAdapter(seatAdapter);

        levisStadiumTextView = (TextView) findViewById(R.id.levisStadiumTextView);

        levisStadiumTextView.setText(stadiums[stadiumIndex]);

        nextScreenTwoButton = (Button) findViewById(R.id.nextScreenTwoButton);

        nextScreenTwoButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int sectionDropdownIndexInt = sectionDropdown.getSelectedItemPosition();
                int rowDropdownIndexInt = rowDropdown.getSelectedItemPosition();
                int seatDropdownIndexInt = seatDropdown.getSelectedItemPosition();
                String sectionDropdownIndex = Integer.toString(sectionDropdownIndexInt);
                String rowDropdownIndex = Integer.toString(rowDropdownIndexInt);
                String seatDropdownIndex = Integer.toString(seatDropdownIndexInt);

                Intent intent = new Intent(getApplicationContext(), ThirdActivity.class);
                intent.putExtra("stadiumIndex", stadiumIndexString);
                intent.putExtra("sectionIndex", sectionDropdownIndex);
                intent.putExtra("rowIndex", rowDropdownIndex);
                intent.putExtra("seatIndex", seatDropdownIndex);

                startActivityForResult(intent, 0);
            }
        });

    }
}

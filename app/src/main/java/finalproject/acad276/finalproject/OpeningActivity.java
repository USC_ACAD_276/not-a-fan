package finalproject.acad276.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class OpeningActivity extends AppCompatActivity {

    Button button;
    EditText userText;
    EditText passText;
    String userName;
    String passWord;

    int userSuccess;
    int passSuccess;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opening_page);

        button = (Button) findViewById(R.id.button);
        userText = (EditText) findViewById(R.id.userText);
        passText = (EditText) findViewById(R.id.passText);






        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String [] passwords = getResources().getStringArray(R.array.passwords);
                String [] usernames = getResources().getStringArray(R.array.usernames);

                userName = userText.getText().toString();
                passWord = passText.getText().toString();
                for (int i = 0; i < passwords.length; i++) {
                    if (passwords[i].equalsIgnoreCase(passWord)) {
                        passSuccess = 1;
                    }
                    if (usernames[i].equalsIgnoreCase(userName)) {
                        userSuccess = 1;
                    }
                }

                if(userSuccess == 1 && passSuccess == 1) {
                    Intent intent = new Intent (getApplicationContext(), AdminActivity.class);

                    startActivityForResult(intent, 0);
                }
                else {
                    Toast.makeText(getApplicationContext(), "Sorry, the entered username and password don't match our records.", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }
}

package finalproject.acad276.finalproject;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    Button saySomethingButton;
    Spinner stadiumDropdown;
    String myValue;
    Button adminLoginButton;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        saySomethingButton = (Button) findViewById(R.id.saySomethingButton);
        adminLoginButton = (Button) findViewById(R.id.adminLoginButton);
        stadiumDropdown = (Spinner) findViewById(R.id.stadiumDropdown);

        final ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this,R.layout.spinner_item, getResources().getStringArray(R.array.stadiums)
        );
        spinnerArrayAdapter.setDropDownViewResource(R.layout.spinner_item);
        stadiumDropdown.setAdapter(spinnerArrayAdapter);

        myValue = "Levi's Stadium (Santa Clara, California)";

        stadiumDropdown.setSelection(13);


        saySomethingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int stadiumDropdownIndexInt = stadiumDropdown.getSelectedItemPosition();

                if (stadiumDropdownIndexInt == 13) {
                    Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
                    String stadiumDropdownIndex = Integer.toString(stadiumDropdownIndexInt);
                    intent.putExtra("stadiumIndex", stadiumDropdownIndex);

                    startActivityForResult(intent, 0);
                } else {
                    String text = stadiumDropdown.getSelectedItem().toString();
                    stadiumDropdown.setSelection(13);
                    Toast.makeText(getApplicationContext(), "Sorry, NotAFan is not yet available at " + text + ".", Toast.LENGTH_SHORT).show();
                }
            }
        });

        adminLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), OpeningActivity.class);

                startActivityForResult(intent, 0);
            }
        });


    }
}

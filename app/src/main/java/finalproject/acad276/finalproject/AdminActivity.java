package finalproject.acad276.finalproject;


        import java.net.HttpURLConnection;
        import java.net.URL;

import android.content.Intent;
        import android.net.Uri;
        import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
        import android.view.View;
import android.widget.Button;
        import android.widget.TextView;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;

public class AdminActivity extends AppCompatActivity {

    Button homeButton;
    TextView textViewReport;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_page);

        textViewReport = (TextView) findViewById(R.id.textViewReport);
        homeButton = (Button) findViewById(R.id.homeButton);

        String url = "http://pmalacho.student.uscitp.com/itp300/wk9/login.php";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
        /*URL url;
        try {
            url = new URL("http://tutorials.jenkov.com/java-exception-handling/basic-try-catch-finally.html");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            try {
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            }
            finally {
                    urlConnection.disconnect();
            }
        } catch (MalformedURLException e) {
            textViewReport.setText("oops");
        } catch (IOException e) {
            textViewReport.setText("oops");
        }*/


        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);

                startActivityForResult(intent, 0);
            }
        });

    }


}





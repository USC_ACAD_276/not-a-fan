package finalproject.acad276.finalproject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class FinalActivity extends AppCompatActivity{
    String stadiumIndexString;
    String sectionIndexString;
    String rowIndexString;
    String seatNumberString;
    String threatLevelString;

    String drunkYes;
    String drunkMsg;
    String obsceneYes;
    String obsceneMsg;
    String rowdyYes;
    String rowdyMsg;
    String loudYes;
    String loudMsg;
    String violentYes;
    String violentMsg;
    String otherBehaviors;
    String otherComments;
    String urlMake;

    TextView textViewFinal;

    Button homeButton;
    Button submitButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.final_page);

        Intent intent = getIntent();
        stadiumIndexString = intent.getStringExtra("stadiumIndex");
        final int stadiumIndex = Integer.parseInt(stadiumIndexString);
        final int stadiumId = (stadiumIndex + 1);
        sectionIndexString = intent.getStringExtra("sectionIndex");
        final int sectionIndex = Integer.parseInt(sectionIndexString);
        final int sectionId = (sectionIndex + 1);
        rowIndexString = intent.getStringExtra("rowIndex");
        final int rowIndex = Integer.parseInt(rowIndexString);
        final int rowId = (rowIndex + 1);
        seatNumberString = intent.getStringExtra("seatNumber");
        final int seatNumber = Integer.parseInt(seatNumberString);
        threatLevelString = intent.getStringExtra("threatLevel");
        final int threatLevel = Integer.parseInt(threatLevelString);
        otherBehaviors = intent.getStringExtra("otherBehaviors");
        otherComments = intent.getStringExtra("otherComments");
        drunkYes = intent.getStringExtra("drunkYes");
        drunkMsg = intent.getStringExtra("drunkMsg");
        obsceneYes = intent.getStringExtra("obsceneYes");
        obsceneMsg = intent.getStringExtra("obsceneMsg");
        rowdyYes = intent.getStringExtra("rowdyYes");
        rowdyMsg = intent.getStringExtra("rowdyMsg");
        loudYes = intent.getStringExtra("loudYes");
        loudMsg = intent.getStringExtra("loudMsg");
        violentYes = intent.getStringExtra("violentYes");
        violentMsg = intent.getStringExtra("violentMsg");

        StringBuilder sb = new StringBuilder(100);
        sb.append("http://pmalacho.student.uscitp.com/itp300/database_final/report.php?section_id=");
        sb.append(sectionId);
        sb.append("&stadium_id=");
        sb.append(stadiumId);
        sb.append("&row_id=");
        sb.append(rowId);
        sb.append("&seat_number=");
        sb.append(seatNumber);
        if (drunkYes.equalsIgnoreCase("1")) {
          sb.append("&drunk=");
          sb.append(drunkYes);
        }
        if (rowdyYes.equalsIgnoreCase("1")) {
            sb.append("&rowdy=");
            sb.append(rowdyYes);
        }
        if (loudYes.equalsIgnoreCase("1")) {
            sb.append("&loud=");
            sb.append(loudYes);
        }
        if (obsceneYes.equalsIgnoreCase("1")) {
            sb.append("&obscene=");
            sb.append(obsceneYes);
        }
        if (violentYes.equalsIgnoreCase("1")) {
            sb.append("&violent=");
            sb.append(violentYes);
        }
        sb.append("&otherBehavior=");
        sb.append(otherBehaviors);
        sb.append("&threat_level=");
        sb.append(threatLevel);
        sb.append("&otherComments=");
        sb.append(otherComments);
        sb.append("&submit=Report");
        urlMake = sb.toString();

        String [] stadiums = getResources().getStringArray(R.array.stadiums);
        String [] sections = getResources().getStringArray(R.array.sections);
        String [] rows = getResources().getStringArray(R.array.rows);

        textViewFinal = (TextView) findViewById(R.id.textViewFinal);

        textViewFinal.setText("Did we get it all right?:\n\n"+
                "Stadium: " + stadiums[stadiumIndex] + "\n"
        + "Section: " + sections[sectionIndex] + "\n"
        + "Row: " + rows[rowIndex] + "\n"
        + "Seat: " + seatNumber + "\n"
        + "Drunk: " + drunkMsg + "\n"
        + "Obscene: " + obsceneMsg + "\n"
        + "Rowdy: " + rowdyMsg + "\n"
        + "Loud: " + loudMsg + "\n"
        + "Violent: " + violentMsg + "\n"
        + "Other Behaviors: " + otherBehaviors + "\n"
        + "Threat Level: " + threatLevel + "\n"
        + "Other Comments: " + otherComments + "\n");


        submitButton = (Button) findViewById(R.id.submitButton);
        homeButton = (Button) findViewById(R.id.homeButton);

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), Submit.class);
                intent.putExtra("url", urlMake);

                startActivityForResult(intent, 0);
            }
        });

        homeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent (getApplicationContext(), MainActivity.class);

                startActivityForResult(intent, 0);
            }
        });


    }
}


package finalproject.acad276.finalproject;

import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import java.util.ArrayList;


public class FifthActivity extends AppCompatActivity{
    String stadiumIndexString;
    String sectionIndexString;
    String rowIndexString;
    String seatNumberString;
    String threatLevelString;
    EditText anythingElseEditText;

    String drunkYes;
    String drunkMsg;
    String obsceneYes;
    String obsceneMsg;
    String rowdyYes;
    String rowdyMsg;
    String loudYes;
    String loudMsg;
    String violentYes;
    String violentMsg;
    String otherBehaviors;
    String otherComments;

    Button reportButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen5);

        Intent intent = getIntent();
        stadiumIndexString = intent.getStringExtra("stadiumIndex");
        sectionIndexString = intent.getStringExtra("sectionIndex");
        rowIndexString = intent.getStringExtra("rowIndex");
        seatNumberString = intent.getStringExtra("seatNumber");
        threatLevelString = intent.getStringExtra("threatLevel");
        otherBehaviors = intent.getStringExtra("otherBehaviors");
        drunkYes = intent.getStringExtra("drunkYes");
        drunkMsg = intent.getStringExtra("drunkMsg");
        obsceneYes = intent.getStringExtra("obsceneYes");
        obsceneMsg = intent.getStringExtra("obsceneMsg");
        rowdyYes = intent.getStringExtra("rowdyYes");
        rowdyMsg = intent.getStringExtra("rowdyMsg");
        loudYes = intent.getStringExtra("loudYes");
        loudMsg = intent.getStringExtra("loudMsg");
        violentYes = intent.getStringExtra("violentYes");
        violentMsg = intent.getStringExtra("violentMsg");

        anythingElseEditText = (EditText) findViewById(R.id.anythingElseEditText);
        reportButton = (Button) findViewById(R.id.reportButton);

        reportButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otherComments = anythingElseEditText.getText().toString();

                Intent intent = new Intent(getApplicationContext(), FinalActivity.class);
                intent.putExtra("stadiumIndex", stadiumIndexString);
                intent.putExtra("sectionIndex", sectionIndexString);
                intent.putExtra("rowIndex", rowIndexString);
                intent.putExtra("seatNumber", seatNumberString);
                intent.putExtra("otherBehaviors", otherBehaviors);
                intent.putExtra("threatLevel", threatLevelString);
                intent.putExtra("drunkYes", drunkYes);
                intent.putExtra("drunkMsg", drunkMsg);
                intent.putExtra("obsceneYes", obsceneYes);
                intent.putExtra("obsceneMsg", obsceneMsg);
                intent.putExtra("rowdyYes", rowdyYes);
                intent.putExtra("rowdyMsg", rowdyMsg);
                intent.putExtra("loudYes", loudYes);
                intent.putExtra("loudMsg", loudMsg);
                intent.putExtra("violentYes", violentYes);
                intent.putExtra("violentMsg", violentMsg);
                intent.putExtra("otherComments", otherComments);

                startActivityForResult(intent, 0);
            }
        });


    }
}

package finalproject.acad276.finalproject;

import android.content.Intent;
import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import java.util.ArrayList;


public class FourthActivity extends AppCompatActivity{

    String stadiumIndexString;
    String sectionIndexString;
    String rowIndexString;
    String seatNumberString;

    Switch drunk;
    Switch obscene;
    Switch rowdy;
    Switch loud;
    Switch violent;
    EditText otherEditText;
    SeekBar threatSeekBar;
    int threatLevel;

    String drunkYes;
    String drunkMsg;
    String obsceneYes;
    String obsceneMsg;
    String rowdyYes;
    String rowdyMsg;
    String loudYes;
    String loudMsg;
    String violentYes;
    String violentMsg;
    String otherBehaviors;

    Button screenFourNextButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen4);

        Intent intent = getIntent();
        stadiumIndexString = intent.getStringExtra("stadiumIndex");
        sectionIndexString = intent.getStringExtra("sectionIndex");
        rowIndexString = intent.getStringExtra("rowIndex");
        seatNumberString = intent.getStringExtra("seatNumber");

        drunk = (Switch) findViewById(R.id.drunkSwitch);
        obscene = (Switch) findViewById(R.id.obsceneSwitch);
        rowdy = (Switch) findViewById(R.id.rowdySwitch);
        loud = (Switch) findViewById(R.id.loudSwitch);
        violent = (Switch) findViewById(R.id.violentSwitch);

        drunkYes = "";
        drunkMsg = "";
        obsceneYes = "";
        obsceneMsg = "";
        rowdyYes = "";
        rowdyMsg = "";
        loudYes = "";
        loudMsg = "";
        violentYes = "";
        violentMsg = "";
        otherBehaviors = "";

        otherEditText = (EditText) findViewById(R.id.otherEditText);

        threatSeekBar = (SeekBar) findViewById(R.id.threatSeekBar);

        screenFourNextButton = (Button) findViewById(R.id.screenFourNextButton);

        screenFourNextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                threatLevel = threatSeekBar.getProgress();
                if (drunk.isChecked()) {
                    drunkYes = "1";
                    drunkMsg = "is drunk";
                }
                if (obscene.isChecked()) {
                    obsceneYes = "1";
                    obsceneMsg = "is obscene";
                }
                if (rowdy.isChecked()) {
                    rowdyYes = "1";
                    rowdyMsg = "is rowdy";
                }
                if (loud.isChecked()) {
                    loudYes = "1";
                    loudMsg = "is loud";
                }
                if (violent.isChecked()) {
                    violentYes = "1";
                    violentMsg = "is violent";
                }

                otherBehaviors = otherEditText.getText().toString();
                String threatLevelString = Integer.toString(threatLevel);

                Intent intent = new Intent(getApplicationContext(), FifthActivity.class);

                intent.putExtra("stadiumIndex", stadiumIndexString);
                intent.putExtra("sectionIndex", sectionIndexString);
                intent.putExtra("rowIndex", rowIndexString);
                intent.putExtra("seatNumber", seatNumberString);
                intent.putExtra("otherBehaviors", otherBehaviors);
                intent.putExtra("threatLevel", threatLevelString);
                intent.putExtra("drunkYes", drunkYes);
                intent.putExtra("drunkMsg", drunkMsg);
                intent.putExtra("obsceneYes", obsceneYes);
                intent.putExtra("obsceneMsg", obsceneMsg);
                intent.putExtra("rowdyYes", rowdyYes);
                intent.putExtra("rowdyMsg", rowdyMsg);
                intent.putExtra("loudYes", loudYes);
                intent.putExtra("loudMsg", loudMsg);
                intent.putExtra("violentYes", violentYes);
                intent.putExtra("violentMsg", violentMsg);

                startActivityForResult(intent, 0);

            }
        });




    }
}

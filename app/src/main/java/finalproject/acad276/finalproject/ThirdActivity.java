package finalproject.acad276.finalproject;

import android.content.DialogInterface;
import android.content.Intent;
import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by parkermalachowsky on 12/11/15.
 */
public class ThirdActivity extends AppCompatActivity{

    TextView vipSectionNumberTextView;

    Button rowOneButtonOne;
    Button rowOneButtonTwo;
    Button rowOneButtonThree;
    Button rowOneButtonFour;
    Button rowOneButtonFive;
    Button rowOneButtonSix;
    Button rowOneButtonSeven;
    Button rowOneButtonEight;
    Button rowOneButtonNine;
    Button rowOneButtonTen;
    Button rowOneButtonEleven;
    Button rowOneButtonTwelve;
    Button rowOneButtonThirteen;
    Button rowOneButtonFourteen;

    Button rowTwoButtonOne;
    Button rowTwoButtonTwo;
    Button rowTwoButtonThree;
    Button rowTwoButtonFour;
    Button rowTwoButtonFive;
    Button rowTwoButtonSix;
    Button rowTwoButtonSeven;
    Button rowTwoButtonEight;
    Button rowTwoButtonNine;
    Button rowTwoButtonTen;
    Button rowTwoButtonEleven;
    Button rowTwoButtonTwelve;
    Button rowTwoButtonThirteen;
    Button rowTwoButtonFourteen;

    Button rowThreeButtonOne;
    Button rowThreeButtonTwo;
    Button rowThreeButtonThree;
    Button rowThreeButtonFour;
    Button rowThreeButtonFive;
    Button rowThreeButtonSix;
    Button rowThreeButtonSeven;
    Button rowThreeButtonEight;
    Button rowThreeButtonNine;
    Button rowThreeButtonTen;
    Button rowThreeButtonEleven;
    Button rowThreeButtonTwelve;
    Button rowThreeButtonThirteen;
    Button rowThreeButtonFourteen;

    Button rowFourButtonOne;
    Button rowFourButtonTwo;
    Button rowFourButtonThree;
    Button rowFourButtonFour;
    Button rowFourButtonFive;
    Button rowFourButtonSix;
    Button rowFourButtonSeven;
    Button rowFourButtonEight;
    Button rowFourButtonNine;
    Button rowFourButtonTen;
    Button rowFourButtonEleven;
    Button rowFourButtonTwelve;
    Button rowFourButtonThirteen;
    Button rowFourButtonFourteen;

    Button rowFiveButtonOne;
    Button rowFiveButtonTwo;
    Button rowFiveButtonThree;
    Button rowFiveButtonFour;
    Button rowFiveButtonFive;
    Button rowFiveButtonSix;
    Button rowFiveButtonSeven;
    Button rowFiveButtonEight;
    Button rowFiveButtonNine;
    Button rowFiveButtonTen;
    Button rowFiveButtonEleven;
    Button rowFiveButtonTwelve;
    Button rowFiveButtonThirteen;
    Button rowFiveButtonFourteen;

    Button nextScreenThreeButton;

    Button selected;
    String select;
    Button current;

    int oneRowIndex;
    int twoRowIndex;
    int fourRowIndex;
    int fiveRowIndex;

    int Index;
    String stadiumIndexString;
    String sectionIndexString;
    String rowIndexString;
    String seatIndexString;

    TextView rowOneTextView;
    TextView rowTwoTextView;
    TextView rowThreeTextView;
    TextView rowFourTextView;
    TextView rowFiveTextView;

    TableLayout rowOneTableLayout;
    TableLayout rowTwoTableLayout;
    TableLayout rowFourTableLayout;
    TableLayout rowFiveTableLayout;

    String buttonText;
    int row;
    int newSeatIndex;
    String newSeatIndexString;

    public void set() {
        ViewGroup parentView = (ViewGroup) findViewById(R.id.rowOneTableRow);
        for(int i=0; i < parentView.getChildCount(); i++) {
            parentView.getChildAt(i).setBackgroundResource(android.R.drawable.btn_default);
        }
        ViewGroup parentView2 = (ViewGroup) findViewById(R.id.rowTwoTableRow);
        for(int i=0; i < parentView2.getChildCount(); i++) {
            parentView2.getChildAt(i).setBackgroundResource(android.R.drawable.btn_default);
        }
        ViewGroup parentView3 = (ViewGroup) findViewById(R.id.rowThreeTableRow);
        for(int i=0; i < parentView3.getChildCount(); i++) {
            parentView3.getChildAt(i).setBackgroundResource(android.R.drawable.btn_default);
        }
        ViewGroup parentView4 = (ViewGroup) findViewById(R.id.rowFourTableRow);
        for(int i=0; i < parentView4.getChildCount(); i++) {
            parentView4.getChildAt(i).setBackgroundResource(android.R.drawable.btn_default);
        }
        ViewGroup parentView5 = (ViewGroup) findViewById(R.id.rowFiveTableRow);
        for(int i=0; i < parentView5.getChildCount(); i++) {
            parentView5.getChildAt(i).setBackgroundResource(android.R.drawable.btn_default);
        }
        selected.setBackgroundColor(Color.WHITE);
        selected.setTextColor(Color.RED);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen3);



        Intent intent = getIntent();
        stadiumIndexString = intent.getStringExtra("stadiumIndex");
        final int stadiumIndex = Integer.parseInt(stadiumIndexString);
        sectionIndexString = intent.getStringExtra("sectionIndex");
        final int sectionIndex = Integer.parseInt(sectionIndexString);
        rowIndexString = intent.getStringExtra("rowIndex");
        final int rowIndex = Integer.parseInt(rowIndexString);
        seatIndexString = intent.getStringExtra("seatIndex");
        final int seatIndex = Integer.parseInt(seatIndexString);

        String [] stadiums = getResources().getStringArray(R.array.stadiums);
        String [] sections = getResources().getStringArray(R.array.sections);
        String [] rows = getResources().getStringArray(R.array.rows);
        String [] seats = getResources().getStringArray(R.array.seats);

        vipSectionNumberTextView = (TextView) findViewById(R.id.vipSectionNumberTextView);
        vipSectionNumberTextView.setText(sections[sectionIndex]);


        rowThreeTextView = (TextView) findViewById(R.id.rowThreeTextView);
        rowTwoTextView = (TextView) findViewById(R.id.rowTwoTextView);
        rowOneTextView = (TextView) findViewById(R.id.rowOneTextView);
        rowFourTextView = (TextView) findViewById(R.id.rowFourTextView);
        rowFiveTextView = (TextView) findViewById(R.id.rowFiveTextView);

        rowOneTableLayout = (TableLayout) findViewById(R.id.rowOneTableLayout);
        rowTwoTableLayout = (TableLayout) findViewById(R.id.rowTwoTableLayout);
        rowFourTableLayout = (TableLayout) findViewById(R.id.rowFourTableLayout);
        rowFiveTableLayout = (TableLayout) findViewById(R.id.rowFiveTableLayout);

        twoRowIndex = rowIndex - 2;
        oneRowIndex = rowIndex -1;
        fourRowIndex = rowIndex + 1;
        fiveRowIndex = rowIndex + 2;

        if (rowIndex >= 2 && rowIndex <= 28) {
            rowThreeTextView.setText("Row " + rows[rowIndex]);
            rowTwoTextView.setText("Row " + rows[oneRowIndex]);
            rowOneTextView.setText("Row " + rows[twoRowIndex]);
            rowFourTextView.setText("Row " + rows[fourRowIndex]);
            rowFiveTextView.setText("Row " + rows[fiveRowIndex]);
        }
        else if (rowIndex == 1) {
            rowOneTableLayout.setVisibility(View.INVISIBLE);
            rowOneTextView.setText("");
            rowTwoTextView.setText("Row " + rows[oneRowIndex]);
            rowThreeTextView.setText("Row " + rows[rowIndex]);
            rowFourTextView.setText("Row " + rows[fourRowIndex]);
            rowFiveTextView.setText("Row " + rows[fiveRowIndex]);
        }
        else if (rowIndex == 0) {
            rowOneTableLayout.setVisibility(View.INVISIBLE);
            rowOneTextView.setText("");
            rowTwoTableLayout.setVisibility(View.INVISIBLE);
            rowTwoTextView.setText("");
            rowThreeTextView.setText("Row " + rows[rowIndex]);
            rowFourTextView.setText("Row " + rows[fourRowIndex]);
            rowFiveTextView.setText("Row " + rows[fiveRowIndex]);
        }
        else if (rowIndex == 29) {
            rowOneTextView.setText("Row " + rows[twoRowIndex]);
            rowTwoTextView.setText("Row " + rows[oneRowIndex]);
            rowThreeTextView.setText("Row " + rows[rowIndex]);
            rowFourTextView.setText("Row " + rows[fourRowIndex]);
            rowFiveTableLayout.setVisibility(View.INVISIBLE);
            rowFiveTextView.setText("");
        }
        else if (rowIndex == 30) {
            rowThreeTextView.setText("Row " + rows[rowIndex]);
            rowTwoTextView.setText("Row " + rows[oneRowIndex]);
            rowOneTextView.setText("Row " + rows[twoRowIndex]);
            rowFourTableLayout.setVisibility(View.INVISIBLE);
            rowFourTextView.setText("");
            rowFiveTableLayout.setVisibility(View.INVISIBLE);
            rowFiveTextView.setText("");
        }

        Toast.makeText(getApplicationContext(), "Click on the seat near you that is a bummer.", Toast.LENGTH_SHORT).show();

        nextScreenThreeButton = (Button) findViewById(R.id.nextScreenThreeButton);

        rowOneButtonOne = (Button) findViewById(R.id.rowOneButtonOne);
        rowOneButtonTwo = (Button) findViewById(R.id.rowOneButtonTwo);
        rowOneButtonThree = (Button) findViewById(R.id.rowOneButtonThree);
        rowOneButtonFour = (Button) findViewById(R.id.rowOneButtonFour);
        rowOneButtonFive = (Button) findViewById(R.id.rowOneButtonFive);
        rowOneButtonSix = (Button) findViewById(R.id.rowOneButtonSix);
        rowOneButtonSeven = (Button) findViewById(R.id.rowOneButtonSeven);
        rowOneButtonEight = (Button) findViewById(R.id.rowOneButtonEight);
        rowOneButtonNine = (Button) findViewById(R.id.rowOneButtonNine);
        rowOneButtonTen = (Button) findViewById(R.id.rowOneButtonTen);
        rowOneButtonEleven = (Button) findViewById(R.id.rowOneButtonEleven);
        rowOneButtonTwelve = (Button) findViewById(R.id.rowOneButtonTwelve);
        rowOneButtonThirteen = (Button) findViewById(R.id.rowOneButtonThirteen);


        rowTwoButtonOne = (Button) findViewById(R.id.rowTwoButtonOne);
        rowTwoButtonTwo = (Button) findViewById(R.id.rowTwoButtonTwo);
        rowTwoButtonThree = (Button) findViewById(R.id.rowTwoButtonThree);
        rowTwoButtonFour = (Button) findViewById(R.id.rowTwoButtonFour);
        rowTwoButtonFive = (Button) findViewById(R.id.rowTwoButtonFive);
        rowTwoButtonSix = (Button) findViewById(R.id.rowTwoButtonSix);
        rowTwoButtonSeven = (Button) findViewById(R.id.rowTwoButtonSeven);
        rowTwoButtonEight = (Button) findViewById(R.id.rowTwoButtonEight);
        rowTwoButtonNine = (Button) findViewById(R.id.rowTwoButtonNine);
        rowTwoButtonTen = (Button) findViewById(R.id.rowTwoButtonTen);
        rowTwoButtonEleven = (Button) findViewById(R.id.rowTwoButtonEleven);
        rowTwoButtonTwelve = (Button) findViewById(R.id.rowTwoButtonTwelve);
        rowTwoButtonThirteen = (Button) findViewById(R.id.rowTwoButtonThirteen);


        rowThreeButtonOne = (Button) findViewById(R.id.rowThreeButtonOne);
        rowThreeButtonTwo = (Button) findViewById(R.id.rowThreeButtonTwo);
        rowThreeButtonThree = (Button) findViewById(R.id.rowThreeButtonThree);
        rowThreeButtonFour = (Button) findViewById(R.id.rowThreeButtonFour);
        rowThreeButtonFive = (Button) findViewById(R.id.rowThreeButtonFive);
        rowThreeButtonSix = (Button) findViewById(R.id.rowThreeButtonSix);
        rowThreeButtonSeven = (Button) findViewById(R.id.rowThreeButtonSeven);
        rowThreeButtonEight = (Button) findViewById(R.id.rowThreeButtonEight);
        rowThreeButtonNine = (Button) findViewById(R.id.rowThreeButtonNine);
        rowThreeButtonTen = (Button) findViewById(R.id.rowThreeButtonTen);
        rowThreeButtonEleven = (Button) findViewById(R.id.rowThreeButtonEleven);
        rowThreeButtonTwelve = (Button) findViewById(R.id.rowThreeButtonTwelve);
        rowThreeButtonThirteen = (Button) findViewById(R.id.rowThreeButtonThirteen);

        rowFourButtonOne = (Button) findViewById(R.id.rowFourButtonOne);
        rowFourButtonTwo = (Button) findViewById(R.id.rowFourButtonTwo);
        rowFourButtonThree = (Button) findViewById(R.id.rowFourButtonThree);
        rowFourButtonFour = (Button) findViewById(R.id.rowFourButtonFour);
        rowFourButtonFive = (Button) findViewById(R.id.rowFourButtonFive);
        rowFourButtonSix = (Button) findViewById(R.id.rowFourButtonSix);
        rowFourButtonSeven = (Button) findViewById(R.id.rowFourButtonSeven);
        rowFourButtonEight = (Button) findViewById(R.id.rowFourButtonEight);
        rowFourButtonNine = (Button) findViewById(R.id.rowFourButtonNine);
        rowFourButtonTen = (Button) findViewById(R.id.rowFourButtonTen);
        rowFourButtonEleven = (Button) findViewById(R.id.rowFourButtonEleven);
        rowFourButtonTwelve = (Button) findViewById(R.id.rowFourButtonTwelve);
        rowFourButtonThirteen = (Button) findViewById(R.id.rowFourButtonThirteen);

        rowFiveButtonOne = (Button) findViewById(R.id.rowFiveButtonOne);
        rowFiveButtonTwo = (Button) findViewById(R.id.rowFiveButtonTwo);
        rowFiveButtonThree = (Button) findViewById(R.id.rowFiveButtonThree);
        rowFiveButtonFour = (Button) findViewById(R.id.rowFiveButtonFour);
        rowFiveButtonFive = (Button) findViewById(R.id.rowFiveButtonFive);
        rowFiveButtonSix = (Button) findViewById(R.id.rowFiveButtonSix);
        rowFiveButtonSeven = (Button) findViewById(R.id.rowFiveButtonSeven);
        rowFiveButtonEight = (Button) findViewById(R.id.rowFiveButtonEight);
        rowFiveButtonNine = (Button) findViewById(R.id.rowFiveButtonNine);
        rowFiveButtonTen = (Button) findViewById(R.id.rowFiveButtonTen);
        rowFiveButtonEleven = (Button) findViewById(R.id.rowFiveButtonEleven);
        rowFiveButtonTwelve = (Button) findViewById(R.id.rowFiveButtonTwelve);
        rowFiveButtonThirteen = (Button) findViewById(R.id.rowFiveButtonThirteen);

        newSeatIndex = seatIndex + 1;
        newSeatIndexString = Integer.toString(newSeatIndex);


        if (rowThreeButtonOne.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonOne;
        }
        if (rowThreeButtonTwo.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonTwo;
        }
        if (rowThreeButtonThree.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonThree;
        }
        if (rowThreeButtonFour.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonFour;
        }
        if (rowThreeButtonFive.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonFive;
        }
        if (rowThreeButtonSix.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonSix;
        }
        if (rowThreeButtonSeven.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonSeven;
        }
        if (rowThreeButtonEight.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonEight;
        }
        if (rowThreeButtonNine.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonNine;
        }
        if (rowThreeButtonTen.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonTen;
        }
        if (rowThreeButtonEleven.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonEleven;
        }
        if (rowThreeButtonTwelve.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonTwelve;
        }
        if (rowThreeButtonThirteen.getText().toString().equalsIgnoreCase(newSeatIndexString)){
            selected = (Button) rowThreeButtonThirteen;
        }

        set();

        rowOneButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);

            }
        });

        rowOneButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);

            }
        });
        rowOneButtonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonEleven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonTwelve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowOneButtonThirteen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });



        rowTwoButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonEleven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonTwelve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowTwoButtonThirteen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = -1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });


        rowThreeButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonEleven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonTwelve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowThreeButtonThirteen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 0;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });


        rowFourButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonEleven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonTwelve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFourButtonThirteen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 1;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });


        rowFiveButtonOne.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonTwo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonThree.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonFour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonFive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonSix.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonSeven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonEight.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonNine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonTen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonEleven.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonTwelve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });
        rowFiveButtonThirteen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                set();
                buttonText = ((Button) v).getText().toString();
                row = 2;

                ((Button) v).setBackgroundColor(Color.RED);
                ((Button) v).setTextColor(Color.WHITE);
            }
        });



        nextScreenThreeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int complaintRowIndex = rowIndex + (row);
                String rowIndexString = Integer.toString(complaintRowIndex);

                Intent intent = new Intent(getApplicationContext(), FourthActivity.class);
                intent.putExtra("stadiumIndex", stadiumIndexString);
                intent.putExtra("sectionIndex", sectionIndexString);
                intent.putExtra("rowIndex", rowIndexString);
                intent.putExtra("seatNumber", buttonText);

                startActivityForResult(intent, 0);
            }
        });


    }
}
